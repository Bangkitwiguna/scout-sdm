import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOurServiceComponent } from './page-our-service.component';

describe('PageOurServiceComponent', () => {
  let component: PageOurServiceComponent;
  let fixture: ComponentFixture<PageOurServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageOurServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOurServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
