import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageOurServiceComponent } from './page-our-service.component';

@NgModule({
  declarations: [PageOurServiceComponent],
  imports: [
    CommonModule
  ],
  exports: [PageOurServiceComponent]
})
export class PageOurServiceModule { }
