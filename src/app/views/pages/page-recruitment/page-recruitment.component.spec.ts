import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRecruitmentComponent } from './page-recruitment.component';

describe('PageRecruitmentComponent', () => {
  let component: PageRecruitmentComponent;
  let fixture: ComponentFixture<PageRecruitmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRecruitmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRecruitmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
