import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRecruitmentComponent } from './page-recruitment.component';

@NgModule({
  declarations: [PageRecruitmentComponent],
  imports: [
    CommonModule
  ],
  exports: [PageRecruitmentComponent]
})
export class PageRecruitmentModule { }
