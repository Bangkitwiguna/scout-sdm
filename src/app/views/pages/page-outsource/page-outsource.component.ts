import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { PageService } from '../../../services/page-service';

@Component({
  selector: 'app-page-outsource',
  templateUrl: './page-outsource.component.html',
  styleUrls: ['./page-outsource.component.css']
})
export class PageOutsourceComponent implements OnInit {
  pagesData: any = [];

  constructor(
    private router: Router,
    private pageService: PageService,
    private route: ActivatedRoute,
  ) {
    // subscribes every changes of your route
    this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
           // scroll to top
           window.scrollTo(0, 0);
        }
     });
    this.fetchPageData()
  }

  ngOnInit() {
  }

  async fetchPageData() {
    const res = await this.pageService.pagesData('services').toPromise();
    const path: any = await new Promise(res =>
      this.route.url.subscribe((path: any) =>  res(path))
      );

    if (path)  {
      if (path.length > 0 && path[0].path === 'outsource') {
        this.pagesData = res.data.data.filter((item: any) => item.page_name === 'Outsourcing');
      } else if (path.length > 0 && path[0].path === 'payroll') {
        this.pagesData = res.data.data.filter((item: any) => item.page_name === 'Payroll Service');
      }
    }
  }

  onNavigate(page: any) {
    this.router.navigate([`${page}`], { skipLocationChange: true });
  }

}
