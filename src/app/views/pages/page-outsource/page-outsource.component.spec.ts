import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOutsourceComponent } from './page-outsource.component';

describe('PageOutsourceComponent', () => {
  let component: PageOutsourceComponent;
  let fixture: ComponentFixture<PageOutsourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageOutsourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOutsourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
