import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageOutsourceComponent } from './page-outsource.component';

@NgModule({
  declarations: [PageOutsourceComponent],
  imports: [
    CommonModule
  ],
  exports: [PageOutsourceComponent]
})
export class PageOutsourceModule { }
