import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageAboutUsComponent } from './page-about-us.component';

@NgModule({
  declarations: [PageAboutUsComponent],
  imports: [
    CommonModule
  ],
  exports: [PageAboutUsComponent]
})
export class PageAboutUsModule { }
