import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageHomeComponent } from './page-home.component';

@NgModule({
  declarations: [PageHomeComponent],
  imports: [
    CommonModule,
  ],
  exports: [PageHomeComponent]
})
export class PageHomeModule { }
