import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagePrivacyPolicyComponent } from './page-privacy-policy.component';

@NgModule({
  declarations: [PagePrivacyPolicyComponent],
  imports: [
    CommonModule
  ],
  exports: [PagePrivacyPolicyComponent]
})
export class PagePrivacyPolicyModule { }
