import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SectionHeaderComponent } from './section-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SectionHeaderComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [SectionHeaderComponent]
})
export class SectionHeaderModule { }
