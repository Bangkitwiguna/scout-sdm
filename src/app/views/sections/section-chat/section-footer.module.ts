import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { SectionChatComponent } from './section-chat.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [SectionChatComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxMaskModule.forRoot(),
  ],
  exports: [SectionChatComponent]
})
export class SectionChatModule { }
